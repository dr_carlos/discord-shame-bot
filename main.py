#!/usr/bin/env python3

import os
from typing import Optional, Union

import interactions
from dotenv import load_dotenv
from interactions import (Attachment, Client, CommandContext, Extension, File,
                          Intents, Member, Message, Option, OptionType,
                          Permissions, Role, Snowflake, User,
                          extension_command)

# Create the bot with access to message content
intents = Intents.DEFAULT | Intents.GUILD_MESSAGE_CONTENT

load_dotenv()
TOKEN = os.getenv("DISCORD_TOKEN")
if TOKEN is None:
    raise RuntimeError("No Discord Token set.")

client = Client(TOKEN, intents=intents)

SHAME_AMOUNT: int = 1


class GuildShameInfo:
    _shamed_users: dict[int, int]
    _shamed_roles: dict[int, int]
    _shamed_everyone: int

    def __init__(self):
        self._shamed_users = {}
        self._shamed_roles = {}
        self._shamed_everyone = 0

    @property
    def users(self) -> dict[int, int]:
        return self._shamed_users

    @property
    def roles(self) -> dict[int, int]:
        return self._shamed_roles

    @property
    def everyone(self) -> int:
        return self._shamed_everyone

    def shame_everyone(self) -> None:
        global SHAME_AMOUNT
        self._shamed_everyone = SHAME_AMOUNT

    def unshame_everyone(self) -> None:
        self._shamed_everyone = 0

    def shame_user(self, id: int) -> None:
        global SHAME_AMOUNT
        self._shamed_users[id] = SHAME_AMOUNT

    def unshame_user(self, id: int) -> None:
        self._shamed_users[id] = 0

    def shame_role(self, id: int) -> None:
        global SHAME_AMOUNT
        self._shamed_roles[id] = SHAME_AMOUNT

    def unshame_role(self, id: int) -> None:
        self._shamed_roles[id] = 0

    def is_shamed(self, message: Message) -> bool:
        if client_user is not None and (user := message.author.id) != client_user.id:
            if self._shamed_users.setdefault(int(user), 0) > 0:
                return True

            if self._shamed_everyone > 0:
                return True

            if type(message.member) is Member:
                for role in message.member.roles:
                    if self._shamed_roles.setdefault(role, 0):
                        return True

        return False

    def __repr__(self) -> str:
        return f"<GuildShameInfo(users={self._shamed_users},roles={self._shamed_roles},everyone={self._shamed_everyone})>"


guilds: dict[int, GuildShameInfo] = {}
client_user: Optional[User] = None


@client.event
async def on_ready():
    global client_user
    client_user = await client.modify()
    print(
        f"{client_user.username} has connected to Discord and is in the following guilds:"
    )

    for guild in client.guilds:
        print(" -", guild.name)
        guilds[int(guild.id)] = GuildShameInfo()


@client.event
async def on_message_create(message: Message):
    global guilds

    try:
        if message.guild_id is not None and guilds[int(message.guild_id)].is_shamed(
            message
        ):
            await message.create_reaction("😞")
            await message.reply(files=File("shame.gif"))
    except KeyError as e:
        pass


@client.command(
    name="shame",
    description=f"Shame a user",
    options=[
        Option(
            type=OptionType.MENTIONABLE,
            name="users",
            description="the user or role to shame",
            required=True,
        )
    ],
    default_member_permissions=Permissions.ADMINISTRATOR,
)
async def shame(context: CommandContext, users: Union[Role, Member]):
    global guilds, SHAME_AMOUNT

    guild = guilds[int(context.guild_id)]
    name: str = users.name

    if type(users) is Member and users.user is not None:
        guild.shame_user(int(users.user.id))
    elif type(users) is Role:
        if name == "@everyone":
            guild.shame_everyone()
        else:
            guild.shame_role(int(users.id))
            name = users.mention

    await context.send(f"Shame to {name}! Their crimes shall be punished!")


@client.command(
    name="unshame",
    description=f"Unshame a user",
    options=[
        Option(
            type=OptionType.MENTIONABLE,
            name="users",
            description="the user or role to unshame",
            required=True,
        )
    ],
    default_member_permissions=Permissions.ADMINISTRATOR,
)
async def unshame(context: CommandContext, users: Union[Role, Member]):
    global guilds

    guild = guilds[int(context.guild_id)]
    name: str = users.name

    if type(users) is Member and users.user is not None:
        guild.unshame_user(int(users.user.id))
    elif type(users) is Role:
        if name == "@everyone":
            guild.unshame_everyone()
        else:
            guild.unshame_role(int(users.id))
            name = users.mention

    await context.send(f"Hark! {name} is forgiven!")


def main():
    client.start()


if __name__ == "__main__":
    main()
